﻿using ChristmasVIP.DataLayer;
using System.Web.Mvc;

namespace ChristmasVIP.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index(string id = null)
        {
            // go to Db and get details based on Id passed in

            ViewBag.Valid = new ReservationDetails().CheckReservation(id);
            ViewBag.Id = id;

            return View();
        }


        
    }
}