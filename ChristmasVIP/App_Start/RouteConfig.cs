﻿using System.Web.Mvc;
using System.Web.Routing;

namespace ChristmasVIP
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            //routes.MapRoute(
            //    name: "Default",
            //    url: "{controller}/{action}/{id}",
            //    defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            //);

            routes.MapRoute(
                name: "Default",
                url: "{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );


            routes.MapRoute(
            name: "Error",
            url: "{controller}/{action}/{id}",
            defaults: new { controller = "Error", action = "Index", id = UrlParameter.Optional }
             );


        }
    }
}