// Get document/window width
function getWidth(){
    var actualWidth = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth || document.body.offsetWidth;
    return actualWidth;
}

// Get document/window height
function getHeight(){
    var actualHeight = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight || document.body.offsetHeight;
    return actualHeight;
}

// function(){
// 	if(getWidth() < 720){
// 		var node = document.getElementsByClassName("fullpage"); 	
//     	node.parentNode.removeChild(node);
// 	}
// }

// Move the apples on MouseMove
function moveApples(e){
	var mouseX = e.pageX;
	var mouseY = e.pageY;
	TweenMax.to(".apple1", 3, {x:(Math.floor((mouseX/30))),ease:Power4.easeOut});
	TweenMax.to(".apple2", 3, {x:(Math.floor(mouseX/40)),ease:Power4.easeOut});
	TweenMax.to([".apple3","#apple5","#apple6"], 3, {x:(Math.floor(mouseX/50)),ease:Power4.easeOut});
	TweenMax.to(".apple4", 3, {x:(Math.floor(mouseX/60)),ease:Power4.easeOut});
}

function parallax(){
	// Scroll the apples at different rates
	TweenMax.to(".bob_1", 0.75, {y:-(Math.floor(pageYOffset*0.4)),ease:Power1.easeOut});
	TweenMax.to(".bob_2", 0.5, {y:-(Math.floor(pageYOffset*0.25)),ease:Power1.easeOut});
	TweenMax.to(".bob_3", 0.25, {y:-(Math.floor(pageYOffset*0.25)),ease:Power1.easeOut});
	TweenMax.to(".bob_4", 0.25, {y:-(Math.floor(pageYOffset*0.25)),ease:Power1.easeOut});
	
	TweenMax.to("#apple5", 0.25, {y:-(Math.floor(pageYOffset*0.25)),ease:Power1.easeOut});
	TweenMax.to("#apple6", 0.25, {y:-(Math.floor(pageYOffset*0.5)),ease:Power1.easeOut});
	
	// Panel block parallax
	TweenMax.to(["#block_1","#block_3"], 0.5, {y:-(Math.floor(pageYOffset*0.5)),ease:Power4.easeOut});
	TweenMax.to([".celebrate_copy"], 0.5, {y:-(Math.floor(pageYOffset*0.5)),ease:Power4.easeOut});
	TweenMax.to(["#block_2_1"], 0.5, {y:-(Math.floor(pageYOffset*0.7)),ease:Power4.easeOut});
	TweenMax.to(["#block_2_2"], 0.5, {y:-(Math.floor(pageYOffset*0.8)),ease:Power4.easeOut});
	TweenMax.to([".vue_du_monde_img"], 0.5, {y:-(Math.floor(pageYOffset*0.3)),ease:Power4.easeOut});	
	TweenMax.to([".date_img"], 0.5, {y:-(Math.floor(pageYOffset*0.3)),ease:Power4.easeOut});
}

// Start the bob function
function appleBob(){
	var appleArray = [".apple1", ".apple2", ".apple3",".apple4"];
	for(var i = 0; i < appleArray.length; i++ ){
		// set a random animation duration, rotation and Y distance
		TweenMax.to(appleArray[i], getRandomRange(6,8), {y:(getRandomRange(20,40)),ease:Linear.easeNone, repeat:-1, yoyo:true});
	}
}

function scrollToTop(){
	window.scrollTo(0, 1);
}

// Random number within set range
function getRandomRange(min, max) {
  return Math.random() * (max - min) + min;
}

// Get element scroll position
function getPosition(element){
	var xPosition = 0;
	var yPosition = 0;
	while(element)  {
		xPosition += (element.offsetLeft - element.scrollLeft + element.clientLeft);
		yPosition += (element.offsetTop - element.scrollTop + element.clientTop);
		element = element.offsetParent;
	}
	return { x: xPosition, y: yPosition };
}

// Support Early IE events
function addEvent(evnt, elem, func) {
	if (elem.addEventListener){  // W3C DOM
		elem.addEventListener(evnt,func,false);
	}else if(elem.attachEvent){ // IE DOM
		elem.attachEvent("on"+evnt, func);
	}else{ // No much to do
     elem[evnt] = func;
  }
}

// Set the events
addEvent("mousemove", document, moveApples);

setTimeout(function(){
	appleBob();	
}, 250); 

// Trigger inital parallax positioning
parallax();

// Trigger parallax function on scroll
window.addEventListener("scroll", parallax);

// 

addEvent('scroll', document.body, function(e) {
    document.body.scrollLeft = 0;
});

function attending(){
	TweenMax.to(["#rsvp_yes","#rsvp_no","#rsvp_yes_foot","#rsvp_no_foot"],0.5,{opacity: 0});
	TweenMax.delayedCall(1,function(){
		// Remove the buttons
		document.getElementById("wrapper").style.textAlign = "center";
		document.getElementById("wrapper").style.width = "90%";
		document.getElementById("wrapper_foot").style.textAlign = "center";
		document.getElementById("wrapper_foot").style.width = "90%";
		document.getElementById("rsvp_yes").style.display = "none";
		document.getElementById("rsvp_no").style.display = "none";
		document.getElementById("rsvp_yes_foot").style.display = "none";
		document.getElementById("rsvp_no_foot").style.display = "none";
		// Display the thankyou message
		document.getElementById("thankyou_attending").style.display = "inline-block";
		document.getElementById("thankyou_attending_foot").style.display = "inline-block";
		TweenMax.to(["#thankyou_attending","#thankyou_attending_foot"],0.5,{opacity: 1, delay:0});
	});
}

function notAttending(){
	TweenMax.to(["#rsvp_yes","#rsvp_no","#rsvp_yes_foot","#rsvp_no_foot"],0.5,{opacity: 0});
	TweenMax.delayedCall(1,function(){
		// Remove the buttons
		document.getElementById("wrapper").style.textAlign = "center";
		document.getElementById("wrapper_foot").style.textAlign = "center";
		document.getElementById("rsvp_yes").style.display = "none";
		document.getElementById("rsvp_no").style.display = "none";
		document.getElementById("rsvp_yes_foot").style.display = "none";
		document.getElementById("rsvp_no_foot").style.display = "none";
		// Display the thankyou message
		document.getElementById("thankyou_not_attending").style.display = "inline-block";
		document.getElementById("thankyou_not_attending_foot").style.display = "inline-block";
		TweenMax.to(["#thankyou_not_attending","#thankyou_not_attending_foot"],0.5,{opacity: 1, delay:0});
	});
}

addEvent("click", document.getElementById("rsvp_yes"),attending);
addEvent("click", document.getElementById("rsvp_no"),notAttending);
addEvent("click", document.getElementById("rsvp_yes_foot"),attending);
addEvent("click", document.getElementById("rsvp_no_foot"),notAttending);


// Scroll back to top
addEvent("click", document.getElementById("scroll_to_top"),scrollToTop);