﻿using ChristmasVIP.DataLayer;
using ChristmasVIP.Models;
using System.Web.Http;

namespace ChristmasVIP.api
{
    public class ConfirmController : ApiController
    {
        // POST: api/Confirm
        public string Post([FromBody]Attendance attendance)
        {
            // Go and set true/false for attending, the below method will return the users first name
            // which is used to display a message after stating if they can go or not.
            return new ReservationDetails().Attending(attendance);
        }
    }
}