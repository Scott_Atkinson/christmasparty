﻿using System.Configuration;

namespace ChristmasVIP
{
    public static class database
    {
        public static string connection()
        {
            return ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        }
    }
}