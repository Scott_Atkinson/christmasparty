﻿using ChristmasVIP.Models;
using Dapper;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace ChristmasVIP.DataLayer
{
    public class ReservationDetails
    {
        public bool CheckReservation(string Id)
        {
            using (var sqlCon = new SqlConnection(database.connection()))
            {
                return sqlCon.Query<int>("[dbo].[CheckUniqueIdProvided]", new { UniqueId = Id }, commandType: CommandType.StoredProcedure).Single() > 0;
            }
        }

        public string Attending(Attendance model)
        {
            using (var sqlCon = new SqlConnection(database.connection()))
            {
                return sqlCon.Query<string>("[dbo].[ConfirmAttendance]", new { UniqueId = model.UniqueId, Attendance = model.Attending }, commandType: CommandType.StoredProcedure).Single().Trim();
            }
        }
    }
}