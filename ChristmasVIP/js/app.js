﻿var myApp = angular.module('christmasvip', []);

myApp.controller("mainController", function ($scope, $http) {
    $scope.confirmRes = function (id) {
        confirm(id, 1);
    };

    $scope.declineRes = function (id) {
        confirm(id, 0);
    };

    $scope.confirmResFoot = function (id) {
        confirm(id, 1);
    };

    $scope.declineResFoot = function (id) {
        confirm(id, 0);
    };

    function confirm(_uniqueId, _attending) {

        var Attendance = {
            UniqueId: _uniqueId,
            Attending: _attending
        };

        $http.post('/api/confirm', Attendance)
           .then(success, onError());
    }

    function success(data) {
        $scope.firstname = data.data;
    }

    function onError() {
    }
});