﻿namespace ChristmasVIP.Models
{
    public class Attendance
    {
        public string UniqueId { get; set; }
        public bool Attending { get; set; }
    }
}